const fs = require('fs')
let filePath = process.argv[2]
let totalLines = 0

function myFunction(){
    fs.readFile(filePath,
        function finish(err, fileContents){
            totalLines = fileContents.toString().split("\n").length - 1
            console.log(totalLines)
        }
    )
}

myFunction()

