const fs = require('fs')
let filePath = process.argv[2]
let totalLines = 0

function myFunction(callback){
    fs.readFile(filePath,
        function finish(err, fileContents){
            totalLines = fileContents.toString().split("\n").length - 1
            callback()
        }
    )
}

function log(){
    console.log(totalLines)
}

myFunction(log)

